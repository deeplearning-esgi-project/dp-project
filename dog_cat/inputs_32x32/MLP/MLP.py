import glob, os
import numpy as np
import random
import h5py
import cv2

from keras.models import Sequential
from keras.layers import Dense, Input, Flatten
from keras.optimizers import SGD
from keras.metrics import *
from keras.callbacks import TensorBoard, ModelCheckpoint

from dog_cat import DIR
from dog_cat.models_logs import LOG_DIR
from dog_cat.models_save import MODEL_DIR

from progressbar import ProgressBar # ;-) get with the flow



size = 32
inputs_size = 'inputs_' + str(size) + 'x' + str(32)

dataset_path = '/home/dim/Documents/5-IBD/DeepLearning/project/data/dog_cat/train'

# resize image width and height
def resize(img, width, height):
    img = cv2.resize(img, (width, height))
    return np.array(img)

# Loading dataset
def load_dataset(dataset_path, format, validation_percent):

    print('Loading datasets...')
    pbar = ProgressBar()

    inputs = []
    labels = []

    # Read directory
    for img_path in pbar(glob.glob(os.path.join(dataset_path, "*." + format))):
        base_name = os.path.basename(img_path)
        file, ext = os.path.splitext(base_name)

        #get image label
        label = os.path.splitext(file)[0]

        # load image
        img = cv2.imread(img_path)
        # resize and return image as numpy array
        img = resize(img, size, size)
        img = img.flatten()

        inputs.append(img) # add to inputs list

        if(label == 'cat'):
            labels.append(1) # cat 1, dog 0
        else:
            labels.append(0)


    # shuffle all
    r = random.random()
    random.shuffle(inputs, lambda: r)
    random.shuffle(labels, lambda: r)

    train_len = (len(inputs) * validation_percent) // 100

    x_test = np.array(inputs[:train_len]).astype('float32') / 255
    y_test = np.array(labels[:train_len])

    x_train = np.array(inputs[train_len:]).astype('float32') / 255
    y_train = np.array(labels[train_len:])

    print('Loading done')

    return x_train, y_train, x_test, y_test

# Build model
# neural network shape as array
#e.g:
# [10,10,1] => hidden 10 - hidden 10 - output 1
# input shape : data input shape
def get_model(neural_shape, input_shape):
    model = Sequential()

    model.add(Dense(neural_shape[0], activation=activation, input_shape=input_shape))
    if len(neural_shape) > 1:
        for nb_neuron in neural_shape[1:]:
            model.add(Dense(nb_neuron, activation=activation))

    return model

output_dim = 1
epochs = 2000
activation = 'sigmoid'
loss = 'mse'
batch_size = 32

# Evaluate model with test data and write score
def evaluate(model_name, model, val_inputs, val_labels):
    scores = model.evaluate(val_inputs, val_labels, verbose=0)
    with open(os.path.join(DIR, 'final_results.csv'), "a+") as fp:
        print(model_name + ',' + str(scores[1] * 100) + '\n', file=fp)

""" process with stocastic gradient descent: neural network architecture evolution
"""
def neural_architecture_evol(shapes_list, val_percent):

    # get dataset
    x_train, y_train, x_test, y_test = load_dataset(dataset_path, 'jpg', val_percent)
    print('Inputs shape ', x_train[0].shape)
    learning_rate = 0.001
    momentum = 0.0

    for neural_shape in shapes_list:

        model_name = 'MLP_' + str(neural_shape) + 'SGD' + str(learning_rate) + str(momentum) + "_ac-" + activation + '_' + loss
        print("Model : ", model_name)

        model_dir = os.path.join(MODEL_DIR, inputs_size)
        if not os.path.exists(model_dir):
            os.mkdir(model_dir)

        model_path = os.path.join(model_dir, model_name)

        model_log_dir = os.path.join(LOG_DIR, inputs_size)
        if not os.path.exists(model_log_dir):
            os.mkdir(model_log_dir)

        model_log_path = os.path.join(model_log_dir, model_name)

        # Callbacks definition
        tensorboard_callback = TensorBoard(model_log_path, write_graph=True)
        checkpoint = ModelCheckpoint(model_path + '.h5', monitor='val_loss', save_best_only=True, mode='auto')

        #Model definition
        model = get_model(neural_shape=neural_shape, input_shape=x_train[0].shape)

        #save model architecture
        model.summary()

        sgd = SGD(lr=learning_rate, momentum=momentum)

        model.compile(optimizer=sgd, loss=loss, metrics=[binary_accuracy])
        # Train
        model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, validation_data=(x_test, y_test), callbacks=[tensorboard_callback, checkpoint])

        # Evaluate final model
        evaluate(model_name, model, x_test, y_test)


learning_rates = [0.0001, 0.001, 0.01, 0.1]
momentums = [0., 0.3, 0.5, 0.8]
training_percent = 30

shapes_list = [
    [2,1],
    [10,1],
    [100,1],


    [2,2,1],
    [10,10,1],
    [100,100,1],

    [2,2,2,1],
    [10,10,10,1],
    [100,100,100,1],

]

neural_architecture_evol(shapes_list, training_percent)




















