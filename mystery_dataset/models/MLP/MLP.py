import glob, os
import numpy as np
import random
import h5py
import cv2
import pickle

from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import SGD
from keras.metrics import *
from keras.callbacks import TensorBoard, ModelCheckpoint

from dog_cat import DIR
from dog_cat.models_logs import LOG_DIR
from dog_cat.models_save import MODEL_DIR

from progressbar import ProgressBar # ;-) get with the flow



size = 32
inputs_size = 'inputs_' + str(size) + 'x' + str(32)


#********************************************TO CHANGE
data_dir = '/home/dim/datasets/dataset-mystere/'
dataset = data_dir + 'dataset_np.data'

# resize image width and height
def resize(img, width, height):
    img = cv2.resize(img, (width, height))
    return np.array(img)

# Loading dataset
def load_dataset(validation_percent):

    print('Loading datasets...')
    (x_train, y_train) = pickle.load(open(dataset, 'rb'))

    assert (len(x_train) == len(y_train))

    x_train = x_train.flatten()
    nb_example = len(x_train)

    # Shuffle example
    r = random.random()
    random.shuffle(x_train, lambda: r)
    random.shuffle(y_train, lambda: r)

    # Split dataset => 70% for training 30% for test
    train_val = (nb_example * validation_percent) // 100
    x_test = x_train[train_val:]
    y_test = y_train[train_val:]

    x_train = x_train[:train_val]
    y_train = y_train[:train_val]

    x_train = x_train.astype('float32') / 255
    x_test = x_test.astype('float32') / 255

    print('Loading done')

    return x_train, y_train, x_test, y_test

# Build model
# neural network shape as array
#e.g:
# [10,10,1] => hidden 10 - hidden 10 - output 1
# input shape : data input shape
def get_model(neural_shape, input_shape):
    model = Sequential()

    model.add(Dense(neural_shape[0], activation=activation, input_shape=input_shape))
    if len(neural_shape) > 1:
        for nb_neuron in neural_shape[1:]:
            model.add(Dense(nb_neuron, activation=activation))

    return model

output_dim = 1
epochs = 2000
activation = 'sigmoid'
loss = 'mse'
batch_size = 32

# Evaluate model with test data and write score
def evaluate(model_name, model, val_inputs, val_labels):
    scores = model.evaluate(val_inputs, val_labels, verbose=0)
    with open(os.path.join(DIR, 'final_results.csv'), "a+") as fp:
        print(model_name + ',' + str(scores[1] * 100) + '\n', file=fp)

""" process with stocastic gradient descent: neural network architecture evolution
"""
def neural_architecture_evol(shapes_list, val_percent):

    # get dataset
    x_train, y_train, x_test, y_test = load_dataset(val_percent)
    print('Inputs shape ', x_train[0].shape)
    learning_rate = 0.001
    momentum = 0.0

    for neural_shape in shapes_list:

        model_name = 'MLP_' + str(neural_shape) + 'SGD' + str(learning_rate) + str(momentum) + "_ac-" + activation + '_' + loss
        print("Model : ", model_name)

        model_dir = os.path.join(MODEL_DIR, inputs_size)
        if not os.path.exists(model_dir):
            os.mkdir(model_dir)

        model_path = os.path.join(model_dir, model_name)

        model_log_dir = os.path.join(LOG_DIR, inputs_size)
        if not os.path.exists(model_log_dir):
            os.mkdir(model_log_dir)

        model_log_path = os.path.join(model_log_dir, model_name)

        # Callbacks definition
        tensorboard_callback = TensorBoard(model_log_path, write_graph=True)
        checkpoint = ModelCheckpoint(model_path + '.h5', monitor='val_loss', save_best_only=True, mode='auto')

        #Model definition
        model = get_model(neural_shape=neural_shape, input_shape=x_train[0].shape)

        #save model architecture
        model.summary()

        sgd = SGD(lr=learning_rate, momentum=momentum)

        model.compile(optimizer=sgd, loss=loss, metrics=[binary_accuracy])
        # Train
        model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, validation_data=(x_test, y_test), callbacks=[tensorboard_callback, checkpoint])

        # Evaluate final model
        evaluate(model_name, model, x_test, y_test)


learning_rates = [0.0001, 0.001, 0.01, 0.1]
momentums = [0., 0.3, 0.5, 0.8]
training_percent = 30

shapes_list = [
    [2,1],
    [10,1],
    [100,1],


    [2,2,1],
    [10,10,1],
    [100,100,1],

    [2,2,2,1],
    [10,10,10,1],
    [100,100,100,1],

]

neural_architecture_evol(shapes_list, training_percent)




















