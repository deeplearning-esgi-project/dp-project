import glob, os
import numpy as np
import random
import h5py
import cv2
import pickle

from keras.models import Sequential
from keras.layers import Dense, Conv2D, MaxPooling2D, Flatten
from keras.optimizers import SGD
from keras.metrics import *
from keras.callbacks import TensorBoard, ModelCheckpoint

from dog_cat import DIR
from dog_cat.models_logs import LOG_DIR
from dog_cat.models_save import MODEL_DIR

size = 32
inputs_size = 'inputs_' + str(size) + 'x' + str(size)

#change for yours
data_dir = '/home/dim/datasets/dataset-mystere/'
dataset = data_dir + 'dataset_np.data'

conv_activation = 'relu'
fully_activation = 'softmax'

# resize image width and height
def resize(img, width, height):
    img = cv2.resize(img, (width, height))
    return np.array(img)

# Loading dataset
def load_dataset(validation_percent):

    print('Loading datasets...')
    (x_train, y_train) = pickle.load(open(dataset, 'rb'))

    assert (len(x_train) == len(y_train))

    x_train = x_train.flatten()
    nb_example = len(x_train)

    # Shuffle example
    r = random.random()
    random.shuffle(x_train, lambda: r)
    random.shuffle(y_train, lambda: r)

    # Split dataset => 70% for training 30% for test
    train_val = (nb_example * validation_percent) // 100
    x_test = x_train[train_val:]
    y_test = y_train[train_val:]

    x_train = x_train[:train_val]
    y_train = y_train[:train_val]

    x_train = x_train.astype('float32') / 255
    x_test = x_test.astype('float32') / 255

    print('Loading done')

    return x_train, y_train, x_test, y_test

# Build model
# convolutional neural network shape as array
# + fully connected shape
#e.g:
# [[16,32,64], [1024,1] ] => conv 16 filter, conv 32 filters, conv 64 filters +   => hidden 1024 , output 1
#
# input shape : data input shape
def get_model(conv_shape, input_shape):
    model = Sequential()
    print(input_shape)
    model.add(Conv2D(conv_shape[0][0], (3, 3), padding='valid', activation=conv_activation, input_shape=input_shape))

    if len(conv_shape[0]) > 1:
        for filters in conv_shape[0][1:]:
            model.add(Conv2D(filters, (3, 3), padding='valid', activation=conv_activation))
            model.add(MaxPooling2D(strides=(2,2)))

    model.add(Flatten())
    for layer in conv_shape[1]:
        model.add(Dense(layer, activation=fully_activation))

    return model

epochs = 2000
batch_size = 32
loss = 'categorical_crossentropy'


# Evaluate model with test data and write score
def evaluate(model_name, model, val_inputs, val_labels):
    scores = model.evaluate(val_inputs, val_labels, verbose=0)
    with open(os.path.join(DIR, 'final_results_server.csv'), "a+") as fp:
        print(model_name + ',' + str(scores[1] * 100) + '\n', file=fp)


""" process with stocastic gradient descent: neural network architecture evolution
"""
def conv_architecture_evol(conv_shapes, val_percent):

    # get dataset
    x_train, y_train, x_test, y_test = load_dataset(val_percent)

    print('Inputs shape ', x_train[0].shape)
    learning_rate = 0.001
    momentum = 0.5

    for conv_shape in conv_shapes:

        #model_name = 'ConvNet_' + str(conv_shape) + 'SGD' + str(learning_rate) + str(momentum) + '_' + loss
        model_name = 'ConvNet_' + str(conv_shape) + 'Adam_' + loss + 'conv_ac(' + conv_activation + ')_fully_ac(' + fully_activation + ')'
        print("Model : ", model_name)

        model_dir = os.path.join(MODEL_DIR, inputs_size)
        if not os.path.exists(model_dir):
            os.mkdir(model_dir)

        model_path = os.path.join(model_dir, model_name)

        model_log_dir = os.path.join(LOG_DIR, inputs_size)
        if not os.path.exists(model_log_dir):
            os.mkdir(model_log_dir)

        model_log_path = os.path.join(model_log_dir, model_name)

        # Callbacks definition
        tensorboard_callback = TensorBoard(model_log_path, write_graph=True)
        checkpoint = ModelCheckpoint(model_path + '.h5', monitor='val_loss', save_best_only=True, mode='auto')

        #Model definition
        model = get_model(conv_shape=conv_shape, input_shape=x_train[0].shape)

        #save model architecture
        model.summary()

        sgd = SGD(lr=learning_rate, momentum=momentum)

        model.compile(optimizer='adam', loss=loss, metrics=[categorical_accuracy])
        # Train
        model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, validation_data=(x_test, y_test), callbacks=[tensorboard_callback, checkpoint])

        # Evaluate final model
        evaluate(model_name, model, x_test, y_test)


validation_percent = 30

conv_list = [
   #[[16], [120, 2]],
    [[16, 32], [120, 2]],
    [[16, 32, 64], [120, 2]],

    [[16, 16, 32, 32, 64, 64], [120, 2]]

]

conv_architecture_evol(conv_list, validation_percent)




















