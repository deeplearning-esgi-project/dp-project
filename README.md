# README #

Deep learning project:

# Classification of Dog versus Cat Kaggle challenge
# Classification of Bird sounds challenge
# Classification of mystery dataset

## 1 - Create environment

 Si vous avez du gpu
        $ conda env create -f environment.yml

 Si vous n'avez pas de Gpu
        $ conda env create -f environment-gpu.yml

# 2 - Lancer le script

    Salut mec, voici les étapes à suivre : sur python 3.5
    - git clone https://CreativeDim@bitbucket.org/deeplearning-esgi-project/dp-project.git
    - cd dp-project/

    Il te faut une machine ou tu peux attribuer au moins 2Go de RAM

    les packages à avoir sont :
    numpy , jupyter, opencv2, h5py, tensorflow ou tensorflow si tu as un gpu et keras version 2.1.2 (normalement avec un --upgrade t'auras tout à jour)

    pour lancer les regressions lineaire:
     mystery_dataset/models/LinearReg/LinearReg.py

        NE LANCEZ PAS LE PROGRAMME AVANT D AVOIR CHANGER LES VARIABLES SUIVANTES:
        Mettez vos propre chemin vers le répertoire de données
        #********************************************TO CHANGE LINE 25-26
        data_dir = '/home/dim/datasets/dataset-mystere/'
        dataset = data_dir + 'dataset_np.data'


     Pour lancer les MLP:
        mystery_dataset/models/MLP/MLP.py

          NE LANCEZ PAS LE PROGRAMME AVANT D AVOIR CHANGER LES VARIABLES SUIVANTES:
        Mettez vos propre chemin vers le répertoire de données
        #********************************************TO CHANGE LINE 27 - 28
        data_dir = '/home/dim/datasets/dataset-mystere/'
        dataset = data_dir + 'dataset_np.data'

     Pour lancer les ConvNet:
        mystery_dataset/models/ConvNet/ConvNet.py

          NE LANCEZ PAS LE PROGRAMME AVANT D AVOIR CHANGER LES VARIABLES SUIVANTES:
        Mettez vos propre chemin vers le répertoire de données
        #********************************************TO CHANGE LINE 22 - 23
        data_dir = '/home/dim/datasets/dataset-mystere/'
        dataset = data_dir + 'dataset_np.data'


     Je vais me reposez un peu je me reconnecte vers 18h en cas de bug dites le moi le plus tôt possible ;-) Peace




