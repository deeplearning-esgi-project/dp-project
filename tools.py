import os
from PIL import Image
import numpy as np
import logging
from logging.handlers import RotatingFileHandler

data_dir = '/Users/Dimitri.UNIMON/Documents/ESGI/DEEP-LEARNING/data/dvc'

def resize_img(img, width, height):
    return np.array(img.resize(width, height))



def resize(origin_path, width, height):
    img = Image.open(origin_path).resize((width, height))
    return img


def save(image, repo_path, filename):
    dir = os.path.join(data_dir, repo_path)
    if not os.path.exists(dir):
        os.mkdir(dir)

    image.save(os.path.join(dir, filename + '.png'), 'png')


def resize_and_save(width, height):

    #print('Create data repository with resized image (' + str(width) + 'x' + str(height) + ') ... ')
    train_dir = os.path.join(data_dir, 'origin', 'train')
    for file in os.listdir(train_dir):

        path = os.path.join(train_dir, file)
        img = resize(path, width, height)
        repo_path = 'inputs_' + str(width) + 'x' + str(height)

        filename = os.path.splitext(file)[0]
        #save(img, repo_path, filename)
    print('Done')

#resize(32, 32)





